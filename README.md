# **RegexGUI**

RegexGUI lets you search the *debug.txt* of your minetest instance through ingame commands.

## Requirements

- default

## Installtion

1. git clone the repository into your mod folder
  
2. enable the mod in your world.mt file
  
3. set these varibals in your minetest.conf
  
  - ``regexgui_debug_path = string``
    
    path to your *debug.txt*
    
  - `regexgui_timeout_limit = int`
    
    How long a regexgui search is allowed to take
    
  - ``regexgui_tail_default = int``
    
    The default settings to how much lines should be displayed in the formspec
    
  - ``regexgui_charFilter = string``
    
    Due to formspec not being able to display square brackets they need to be filterd out. Howevery you could filter words or private information out as well. Use this value if you dont know how to write your own``"\"s/[[]/{/g; s/[]]/}/g\""``
    

## Usage

To use the mod ``mod_security``needs to be turned off

## License

[MIT Copyright (c) 2022 tomraceror](https://gitlab.com/Tomracer/regexgui/-/blob/master/LICENSE)
