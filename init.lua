--Change these settings in the minetest.conf file
local debugPath = minetest.settings:get("regexgui_debug_path") or "/home/user/.minetest/debug.txt"
local timeoutLimit = minetest.settings:get("regexgui_timeout_limit") or "30"
local tailDefault = minetest.settings:get("regexgui_tail_default") or "20"
local charFilter = minetest.settings:get("regexgui_charFilter") or "\'s/[[]/{/g; s/[]]/}/g\'"

local regexMenu = {}

-- check if the world/server administrator turned mod_security off
if not io.popen then
    error("\n\nMod_security is still active. Please turn it off\n")
end


function string:tsplit(del)
    local split = {}
    local ostring = self

    while ostring:find(del) do
        local a, b = ostring:find(del)
        table.insert(split, string.sub(ostring, 1, b-1))
        ostring = string.sub(ostring, b+1)
    end
    table.insert(split, ostring)
    return split
end


function regexMenu.show_formspec(player, formname, resultLabel, tail, keywords, regex, X, Y, Z)
    if resultLabel == nil or #resultLabel <= 1 then
        resultLabel = "Nothing found"
    end
    
    local formspec = {
        "formspec_version[4]",
        "size[17,15]",
        "label[5.2,0.4;Search the Debug.txt with regular expressions]",
        "label[0.6,2;Keywords:]",
        "field[2.5,1.6;10.3,0.8;keywords;;"..keywords.."]",
        "label[0.6,3.5;Coordinats              X:]",
        "field[3.4,3.1;2.5,0.8;X;;"..X.."]",
        "label[6.3,3.5;Y:]",
        "field[6.8,3.1;2.5,0.8;Y;;"..Y.."]",
        "label[9.7,3.5;Z:]",
        "field[10.3,3.1;2.5,0.8;Z;;"..Z.."]",
        "label[0.6,5.1;Regex:]",
        "field[2.5,4.7;10.3,0.8;regex;(other options will be ignored);"..regex.."]",
        "field[14,4.7;2.3,0.8;tail;n*lines    (*);"..tail.."]",
        "button_exit[15.8,0.1;1,0.8;exit;X]",
        "button[6.1,6.1;3,0.8;search;Search]",
        "textarea[1,8;15,6.1;results;Results;"..resultLabel.."]"
    }

    return minetest.show_formspec(player, formname, table.concat(formspec, ""))
end

minetest.register_chatcommand("debug", {
    func = function(player_name)
        regexMenu.show_formspec(player_name, "regexMenu:debug", nil, tailDefault,"","", "", "", "")
    end
})


minetest.register_on_player_receive_fields(function(player, formname, fields)
    if formname ~= "regexMenu:debug" then
        return
    end 

    if fields.search and #fields.regex >= 1 then
        if string.find(fields.regex, " ") then
            local res = "Regular expression is not valid, one or more spaces found."
        else
            local cmd = "timeout "..timeoutLimit.." grep "..fields.regex.." "..debugPath.." | tail -n"..fields.tail.."| sed "..charFilter
            local shell = io.popen(cmd)
            res = shell:read("*a")
            shell:close()
        end
        return regexMenu.show_formspec(player:get_player_name(), "regexMenu:debug", res, fields.tail, fields.keywords, fields.regex, fields.X, fields.Y, fields.Z)
    end

    if fields.search and not (fields.X and fields.Y and fields.Z and fields.keywords)  then
            local res = "No search arguments specified."
            return regexMenu.show_formspec(player:get_player_name(), "regexMenu:debug", res, fields.tail, fields.keywords, fields.regex)

    elseif (fields.X and fields.Y and fields.Z) or fields.keywords then
        -- remove spaces if there are any
        fields.X = string.gsub(fields.X, "%s+", "")
        fields.Y = string.gsub(fields.Y, "%s+", "")
        fields.Z = string.gsub(fields.Z, "%s+", "")
        local pattern = ""
        local keywords = fields.keywords:split(",")
        for key, keyword in pairs(keywords) do
            keyword = string.gsub(keyword, "%s+", "")
            if #keyword >= 1 then pattern = pattern..keyword.."\\|" end
        end
        if #fields.X >= 1 and #fields.Y >= 1 and #fields.Z >=1 then
            pattern = pattern..fields.X..","..fields.Y..","..fields.Z.."\\|"
        end
        local cmd = "timeout "..timeoutLimit.." grep '"..pattern:sub(1, #pattern-2).."' "..debugPath.." | tail -n "..fields.tail.."| sed "..charFilter
        print(cmd)
        local shell = io.popen(cmd)
        local res = shell:read("*a")
        shell:close()
        return regexMenu.show_formspec(player:get_player_name(), "regexMenu:debug", res, fields.tail, fields.keywords, fields.regex, fields.X, fields.Y, fields.Z)

    end
end)
